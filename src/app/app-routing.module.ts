import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "@nativescript/angular";
import { LayoutComponent } from "./components/layout/layout.component";
import { ContainerComponent } from "./components/pages/container/components/container.component";
import { DetailsComponent } from "./components/pages/policies/components/details/details.component";
import { PetitionsPoliciesComponent } from "./components/pages/policies/components/petitions/petitions.component";
import { ViewComponent } from "./components/pages/policies/components/view/view.component";

import { AuthGuard } from "./guard/auth.guard";
import { PrivacityComponent } from "./shared/components/privacity/privacity.component";

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: '/container',
                pathMatch: 'full'
            },
            {
                path: 'container',
                canActivate: [AuthGuard],
                component: ContainerComponent,
                children: [
                    {
                        path: '',
                        loadChildren: () => import('~/app/components/pages/pages.module').then(m => m.PagesModule)
                    }
                ]
            },
            {
                path: 'policies/petitions',
                component: PetitionsPoliciesComponent
            },
            {
                path: 'policies/view/:file',
                component: ViewComponent
            },
            {
                path: 'policies/details/:itempolicie',
                component: DetailsComponent
            },
            {
                path: 'login',
                loadChildren: () => import('~/app/components/login/login.module').then((m) => m.LoginModule)
            },
            {
                path: 'privacity',
                component: PrivacityComponent
            }
        ]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }

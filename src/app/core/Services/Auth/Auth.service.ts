import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings } from '@nativescript/core';
import { Observable } from 'rxjs';
import { environment } from '~/environment/environment';
import { LoginModel } from '../../Models/Login/Login@Model';
import { UserModel } from '../../Models/User/User@Model';

@Injectable({ providedIn: 'root' })
export class AuthService {

    private userActive: number;

    constructor(
        private http: HttpClient,
        private routerExtensions: RouterExtensions
    ) { }

    logIn(loginModel: LoginModel): Observable<UserModel> {
        return this.http.post<UserModel>(`${environment.apiNode}/login`, loginModel);
    }

    hasUserActive() {
        if (ApplicationSettings.getNumber('userActive')) {
            this.userActive = Number(ApplicationSettings.getNumber('userActive'))
            if (this.userActive > -1) {
                return true;
            }

            return false;
        }
    }

}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '~/environment/environment';
import { ClientModel } from '../../Models/Client/Client@Model';

@Injectable({ providedIn: 'root' })
export class DataClientService {

    constructor(
        private http: HttpClient
    ) { }

    getDataClient(clientID: number) {
        return this.http.get<ClientModel>(`${environment.apiMark}/v1/cliente/${clientID}`);
    }

}
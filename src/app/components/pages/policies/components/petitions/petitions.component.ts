import { Component, OnInit } from '@angular/core';
import { Page } from '@nativescript/core';
import { getRootView } from '@nativescript/core/application';
import { RadSideDrawer } from 'nativescript-ui-sidedrawer';

@Component({
    selector: 'app-petitions-component',
    templateUrl: 'petitions.component.html',
    styleUrls: ['petitions.component.scss']
})

export class PetitionsPoliciesComponent implements OnInit {

    titleActionBar: string = 'Mis Solicitudes';

    constructor(
        private page: Page
    ) {
        this.page.actionBarHidden = true;
    }

    ngOnInit() { }

    showDrawer() {
        const sideDrawer = <RadSideDrawer>getRootView();
        sideDrawer.showDrawer();
    }

}
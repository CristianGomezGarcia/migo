import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouterExtensions } from '@nativescript/angular';

import { Utils } from '@nativescript/core';

import * as Permissions from 'nativescript-permissions';
import { environment } from '~/environment/environment';

declare const android: any;

@Component({
    selector: 'app-view',
    templateUrl: 'view.component.html',
    styleUrls: ['view.component.scss']
})

export class ViewComponent implements OnInit {

    titleActionBar: string = 'Mi Póliza';
    PDF_URL: string;
    PDFComplete: boolean = false;

    constructor(
        private activateRoute: ActivatedRoute,
        private routerExtensions: RouterExtensions
    ) {
        const params = JSON.parse(this.activateRoute.snapshot.params.file);
        this.PDF_URL = `${environment.apiNode}/poliza/${params['file']}`;
        this.titleActionBar = `Póliza No.: ${params['policieNumber']}`;
    }

    ngOnInit() { }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    onLoad() {
        this.PDFComplete = true;
    }

    downLoadPolicie() {
        Permissions.requestPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .then((response) => {
                Utils.openUrl(this.PDF_URL);
                this.goBack();
            })
            .catch((error) => {
                console.error(error);
            });
    }
}
import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { isAndroid, isIOS } from '@nativescript/core';

import * as TNSPhone from 'nativescript-phone';

@Component({
    selector: 'app-concentrator-account',
    templateUrl: 'concentratoraccount.component.html'
})

export class ConcentratorAccountComponent implements OnInit {

    titleActionBar: string = 'Cuentas Concentradoras';
    PDF_URL: string = '';

    isIOS: boolean;
    isAndroid: boolean;

    insurersGroupCollection: any;

    supportPhone: string = '5536024275';

    constructor(
        private routerExtensions: RouterExtensions
    ) {
        this.isIOS = isIOS;
        this.isAndroid = isAndroid;
        this.insureCollection();
    }

    ngOnInit() { }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    private insureCollection() {
        this.insurersGroupCollection = [
            {
                id: 'ABA',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'aba.svg',
                document: 'abaSeguros.pdf',
                height: '125px',
                width: '350px'
            },
            {
                id: 'SEGUROS AFIRME',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'afirme.svg',
                document: '',
                height: '255px',
                width: '350px'
            },
            {
                id: 'ANA SEGUROS',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'ana.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'AXA',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'axa.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'BANORTE',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'banorte.svg',
                document: 'banorte.pdf',
                height: '125px',
                width: '350px'
            },
            {
                id: 'GENERAL DE SEGUROS',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'general.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'GNP',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'gnp.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'HDI',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'hdi.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'INBURSA',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'inbursa.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'MAPFRE',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'mapfre.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'MIGO',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'migo.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'EL POTOSI',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'elpotosi.svg',
                document: '',
                height: '255px',
                width: '350px'
            },
            {
                id: 'QUALITAS',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'qualitas.svg',
                document: 'qualitas.pdf',
                height: '125px',
                width: '350px'
            },
            {
                id: 'ZURA',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'sura.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'ZURICH',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'zurich.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'EL AGUILA',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'elaguila.svg',
                document: 'aguilaSeguros.pdf',
                height: '125px',
                width: '350px'
            },
            {
                id: 'AIG',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'aig.svg',
                document: '',
                height: '125px',
                width: '350px'
            },
            {
                id: 'LA LATINO',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'lalatino.svg',
                document: '',
                height: '255px',
                width: '350px'
            },
            {
                id: 'Atlas Seguros',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'atlas.svg',
                document: 'atlasSeguros.pdf',
                height: '255px',
                width: '350px'
            },
            {
                id: 'Bx+',
                success: 'Recuerda compartirnos tu comprobante de pago',
                wrong: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al:',
                imageName: 'bxmas.svg',
                document: 'bxmas.pdf',
                height: '255px',
                width: '350px'
            }
        ]
    }

    call() {
        if (isAndroid) {
            TNSPhone.requestCallPermission()
                .then((response) => {
                    TNSPhone.dial(this.supportPhone, true);
                })
                .catch((error) => {
                    TNSPhone.dial(this.supportPhone, false);
                });
        } else if (isIOS) {
            TNSPhone.dial(this.supportPhone, true)
        }
    }

    viewDocument(documentName: string): void {
        this.routerExtensions.navigate(['/support/concentrator-account/view-document', documentName], {
            transition: {
                name: 'slide'
            }
        })
    }
    
}
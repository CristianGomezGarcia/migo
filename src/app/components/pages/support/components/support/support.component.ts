import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { PetitionClass } from '~/app/core/Classes/Policies/petitions/Petition@Class';

import * as TNSPhone from 'nativescript-phone';
import { isAndroid, isIOS, Page, Utils } from '@nativescript/core';

@Component({
    selector: 'app-support-component',
    templateUrl: 'support.component.html',
    styleUrls: ['support.component.scss']
})

export class SupportComponent implements OnInit {

    petitionsArr: Array<any>;
    isAccordionOpen: boolean = false;
    idToOpen: number = -1;

    insurersGroupCollection: Array<{ id: string, nota1: string, nota2: string, urlIMG: string, documento: string, active: boolean }>;


    petitionClass: PetitionClass = new PetitionClass();
    images: Array<any>;

    isAndroid: any;
    isIOS: any;

    constructor(
        private routerExtensions: RouterExtensions,
        private page: Page
    ) {
        this.page.actionBarHidden = true;
        this.isAndroid = isAndroid;
        this.isIOS = isIOS;
    }

    ngOnInit() {
        this.petitionsArr = this.petitionClass.petitionsArr;
        this.selectImage();
    }

    goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    actionAccordion(id: number) {
        console.log(this.idToOpen);
        if (this.idToOpen === id) {
            if (this.isAccordionOpen) {
                this.isAccordionOpen = false;
            } else {
                this.isAccordionOpen = true;
            }
        } else {
            this.idToOpen = id;
            this.isAccordionOpen = true;
        }
    }



    call(phone: string) {
        if (isAndroid) {
            console.log('Is Android');
            TNSPhone.requestCallPermission()
                .then((response) => {
                    TNSPhone.dial(phone, true);
                })
                .catch((error) => {
                    TNSPhone.dial(phone, false);
                });
        }
        else if (isIOS) {
            TNSPhone.dial(phone, true);
            console.log('Es iOS');
        }
    }

    selectImage() {
        const url: string = `~/assets/iconos-aseguradoras`;
        this.images = [
            { alias: "ABA", name: "aba.png", tel: '800 712 2828', phone: '8007122828' },
            { alias: "SEGUROS AFIRME", name: "afirme.png", tel: '800 723 4763', phone: '8007234763' },
            { alias: "ANA SEGUROS", name: "ana.png", tel: '800 911 2627 ', phone: '8009112627' },
            { alias: "AXA", name: "axa.png", tel: '800 900 1292', phone: '8009001292' },
            { alias: "BANORTE", name: "banorte.png", tel: '800 500 1500', phone: '8005001500' },
            { alias: "GENERAL DE SEGUROS", name: "general.png", tel: '800 472 7696', phone: '8004727696' },
            { alias: "GNP", name: "gnp.png", tel: '800 400 9000', phone: '8004009000' },
            { alias: "HDI", name: "hdi.png", tel: '800 019 6000', phone: '8000196000' },
            { alias: "INBURSA", name: "inbursa.png", tel: '800 909 0000', phone: '8009090000' },
            { alias: "MAPFRE", name: "mapfre.png", tel: '55 5950 7777 ', phone: '5559507777 ' },
            { alias: "MIGO", name: "migo.jpg", tel: '800 00 90 000', phone: '8000090000' },
            { alias: "EL POTOSI", name: "elpotosi.png", tel: '800 00 90 000', phone: '8000090000' },
            { alias: "QUALITAS", name: "qualitas.png", tel: '800 800 2880', phone: '8008002880' },
            { alias: "ZURA", name: "sura.png", tel: '800 911 7692', phone: '8009117692' },
            { alias: "ZURICH", name: "zurich.png", tel: '800 288 6911', phone: '8002886911' },
            { alias: "EL AGUILA", name: "elaguila.png", tel: '800 705 8800', phone: '8007058800' },
            { alias: "AIG", name: "aig.png", tel: '800 001 1300', phone: '8000011300' },
            { alias: "LA LATINO", name: "lalatino.png", tel: '800 685 1170', phone: '8006851170' }
        ];

        /* return `${url}/${images.find((e) => e.alias === alias).name}`; */
    }

    openEmail(email: string) {
        const mailto: string = `mailto:${email}`;
        Utils.openUrl(mailto);
    }

}
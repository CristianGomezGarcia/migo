import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { ConcentratorAccountComponent } from './components/accounts/concentratoraccount.component';


@NgModule({
    imports: [
        NativeScriptCommonModule
    ],
    exports: [],
    declarations: [
        ConcentratorAccountComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class SupportModule { }

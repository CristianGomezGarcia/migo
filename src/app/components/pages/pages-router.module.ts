import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from '@nativescript/angular';
import { LogoutComponent } from '../logout/components/logout.component';
import { PetitionsComponent } from './petitions/components/petitions.component';
import { PoliciesComponent } from './policies/components/policies/policies.component';
import { PetitionsPoliciesComponent } from './policies/components/petitions/petitions.component';
import { ProfileComponent } from './profile/components/profile.component';
import { SupportComponent } from './support/components/support/support.component';

const routes: Routes = [
    {
        path: '',
        component: PoliciesComponent
    },
    {
        path: 'policies/petitions',
        component: PetitionsPoliciesComponent
    },
    {
        path: 'petitions',
        component: PetitionsComponent
    },
    {
        path: 'profile',
        component: ProfileComponent
    },
    {
        path: 'support',
        component: SupportComponent
    },
    {
        path: 'logout',
        component: LogoutComponent
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class PagesRoutingModule { }

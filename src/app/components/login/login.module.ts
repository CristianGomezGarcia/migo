import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { LoginRoutingModule } from "./login-routing.module";
/* import { SharedModule } from "../../shared/shared.module"; */
import { ReactiveFormsModule } from "@angular/forms";
import { LoginComponent } from "./components/login.component";
import { NativeScriptCommonModule, NativeScriptFormsModule } from "@nativescript/angular";
import { SharedModule } from "~/app/shared/shared.module";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        LoginRoutingModule,
        SharedModule,
        ReactiveFormsModule,
        NativeScriptFormsModule
    ],
    exports: [],
    declarations: [
        LoginComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class LoginModule { }

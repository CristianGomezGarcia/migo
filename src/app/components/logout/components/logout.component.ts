import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from '@nativescript/angular';
import { ApplicationSettings } from '@nativescript/core';

@Component({
    selector: 'app-logout',
    templateUrl: 'logout.component.html',
    styleUrls: ['logout.component.scss']
})

export class LogoutComponent implements OnInit {

    textLoader: string = 'Cerrando Sesión';
    logoutStatus: boolean = true;

    constructor(
        private routerExtension: RouterExtensions
    ) { }

    ngOnInit() {
        this.LogOut();
    }

    private LogOut() {
        ApplicationSettings.remove('userActive');
        ApplicationSettings.remove("privacitiy");
        setTimeout(() => {
            this.routerExtension.navigate(['/login'], {
                transition: {
                    name: 'fade'
                },
                clearHistory: true
            });
        }, 1000);
    }
}